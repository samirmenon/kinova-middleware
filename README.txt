A version of the kinova middleware provided to
the Stanford Robotics Lab.
2016-06-01

This is supported to work with the Kinova Jaco 6DOF robot with a 3 finger hand.

Appended with documentation and a robot graphics specification.


==========================
Installation instructions:

Go to the Ubuntu/32 or 64 bit directory and read the readme file.
